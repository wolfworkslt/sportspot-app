import { createStackNavigator, withNavigationFocus } from 'react-navigation';
import HomeScreen from '../screens/HomeScreen';
import PaymentsScreen from '../screens/PaymentsScreen';
import ProgramsScreen from '../screens/ProgramsScreen';
import ProfileScreen from '../screens/ProfileScreen';
import ArticleScreen from '../screens/ArticleScreen';

const Routes = createStackNavigator(
  {
    Home: { screen: HomeScreen },
    Payments: { screen: PaymentsScreen },    
    Programs: { screen: ProgramsScreen},
    Profile: { screen: ProfileScreen },
    Article: { screen: ArticleScreen },
  },
  {
    headerMode: 'screen',
    initialRouteName: 'Home',
    cardStyle: {
      shadowColor: 'transparent',
    },
    navigationOptions: {
      headerStyle: {
        shadowOpacity: 0,
      },
    },
  }
);

export default Routes;