import { Permissions, Constants } from 'expo';
import { AsyncStorage } from "react-native"
import NavigationService from '../navigation/NavigationService';

export default class CardNumber {

    static async checkForCardNo() {
    
        let result = null;    
        const value = await AsyncStorage.getItem('Card:key');

        console.log('AsyncStorage', value);

        if (value !== null) {
            result = value;
        }

        return result;          
    }

    static async storeCardNo(number) {
       await AsyncStorage.setItem('Card:key', number);
    } 
}