import axios from 'axios';
import Config from '../config/env';

const client = axios.create({
  baseURL: Config.apiURL,
  headers: {
    'X-Auth': Config.apiKEY,
  },
});

export default class ApiClient {

    static getMemberData(card) {
      console.log('url', Config.apiURL+`member/?card=${card}`);
      return client.get(`member/?card=${card}`);
    }

    static checkCardNo(card) {
      console.log('url', Config.apiURL+`checkcard/?card=${card}`);
      return client.get(`checkcard/?card=${card}`);
    }  

    static getMemberPayments(card) {
      console.log('url', Config.apiURL+`payments/?card=${card}`);
      return client.get(`payments/?card=${card}`);
    }  

    static getMemberPrograms(card) {
      console.log('url', Config.apiURL+`training/?card=${card}`);
      return client.get(`training/?card=${card}`);
    }  

    
    // static sendAnswer(data) {
    //   return client.post('guess', data);
    // } 
    
    // static noSelfie(data) {
    //   return client.post('noselfie', data);
    // } 
}    