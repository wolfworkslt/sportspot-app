import { NavigationActions } from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  return (
    _navigator &&
    _navigator.dispatch(
      NavigationActions.navigate({
        routeName,
        params,
      })
    )
  );
}

function goBack() {
  return _navigator && _navigator.dispatch(NavigationActions.back());
}

export default {
  navigate,
  goBack,
  setTopLevelNavigator,
};
