import React from 'react';
import { StyleSheet, FlatList, View, Text, TouchableOpacity } from 'react-native';
import ApiClient from '../helpers/ApiClient';
import LoadingScreen from './LoadingScreen';

export default class PaymentsScreen extends React.Component {
  static navigationOptions = {
    title: 'Paskutiniai mokėjimai už narystę',
  };

  state = {  
    loading: true,
    cardNo: null,
    data:  null,
  }; 

  constructor(props) {
    super(props);
  } 
  
  componentDidMount() {
    const { navigation } = this.props;

    this.setState({
      cardNo: navigation.getParam('cardNo', null),
      loading: true
      }, () => {
        this.fetchData();
    });
  }   

  componentWillMount() {
  
    this.setState({
      cardNo: null,
      loading: false,
      data: null,
    });
  }

  async fetchData() {
    
    const cardNo = this.state.cardNo;
    if (cardNo) {
      await ApiClient.getMemberPayments(cardNo).then(res => {
        this.setState({
          data: res.data.data,
          loading: false
        });  
      });
    }
  } 
  
  _onRefresh = () => {
    this.state.loading = true;
    this.fetchData();
  } 

  render() {
    if (this.state.loading) {
      return <LoadingScreen />;
    } else {
      return (
        <View style={styles.container}>
          <FlatList
            data={this.state.data}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) =>
            <View style={styles.eventList}>  
                <TouchableOpacity>
                  <View style={styles.eventBox}>
                    <View style={styles.eventContent}>
                      <Text  style={styles.eventTime}>Mokėjimo data: {item.paydDate}</Text>
                      <Text  style={styles.userName}>Suma: {item.amount} Eur</Text>
                      <Text  style={styles.description}>Periodas: {item.paydFrom} - {item.paydUntill}</Text>
                    </View>
                  </View>
                </TouchableOpacity>            
            </View>
            }
            keyExtractor={item => item.id+''}
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container:{
    backgroundColor: "#4682B4",
  },
  eventList:{
    marginTop:10,
  },
  eventBox: {
    padding:10,
    // marginTop:5,
    marginBottom:5,
    flexDirection: 'row',
  },
  eventDate:{
    flexDirection: 'column',
  },
  eventDay:{
    fontSize:50,
    color: "#0099FF",
    fontWeight: "600",
  },
  eventMonth:{
    fontSize:16,
    color: "#0099FF",
    fontWeight: "600",
  },
  eventContent: {
    flex:1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    // marginLeft:10,
    backgroundColor: '#FFFFFF',
    padding:10,
    borderRadius:10
  },
  description:{
    fontSize:15,
    color: "#646464",
  },
  eventTime:{
    fontSize:18,
    color:"#151515",
  },
  userName:{
    fontSize:16,
    color:"#151515",
  },
});