import React from 'react';
import { StyleSheet, FlatList, View, Text, TouchableOpacity, ScrollView } from 'react-native';
import LoadingScreen from './LoadingScreen';
import NavigationService from '../navigation/NavigationService';

export default class ArticleScreen extends React.Component {
  static navigationOptions = {
   header: null,
  };

  state = {  
    loading: true,
    title: null,
    content: null
  }; 

  constructor(props) {
    super(props);
  } 
  
  componentDidMount() {
    const { navigation } = this.props;

    this.setState({
      title: navigation.getParam('title', null),
      content: navigation.getParam('content', null),
      loading: false
    });
  }     

  componentWillMount() {
  
    this.setState({
      loading: false,
      title: null,
      content: null      
    });
  }
  
  goBack() {
    NavigationService.navigate('Programs', {});
  }

  render() {
    if (this.state.loading) {
      return <LoadingScreen />;
    } else {
        return (
            <ScrollView>
              <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerTitle}>
                      {this.state.title}
                    </Text>
                </View>
      
                <View style={styles.postContent}>
                    <Text style={styles.postTitle}>
                         {this.state.title}
                    </Text>
      
                    <Text style={styles.postDescription}>
                        {this.state.content}
                    </Text>

                    <TouchableOpacity style={styles.shareButton}  onPress={()=> {this.goBack()} }>
                      <Text style={styles.shareButtonText}>Į sąrašą</Text>  
                    </TouchableOpacity> 
                </View>
              </View>
            </ScrollView>
        );        
    }
  }
}

const styles = StyleSheet.create({
    container:{
      flex:1,
    },
    header:{
      padding:30,
      alignItems: 'center',
      backgroundColor: "#87CEEB",
    },
    headerTitle:{
      fontSize:30,
      color:"#FFFFFF",
      marginTop:10,
    },
    name:{
      fontSize:22,
      color:"#FFFFFF",
      fontWeight:'600',
    },
    postContent: {
      flex: 1,
      padding:30,
    },
    postTitle:{
      fontSize:26,
      fontWeight:'600',
    },
    postDescription:{
      fontSize:16,
      marginTop:10,
    },
    date:{
      color: '#696969',
      marginTop:10,
    },
    avatar: {
      width: 80,
      height: 80,
      borderRadius: 35,
      borderWidth: 4,
      borderColor: "#00BFFF",
    },
    profile:{
      flexDirection: 'row',
      marginTop:20
    },
    name:{
      fontSize:22,
      color:"#00BFFF",
      fontWeight:'600',
      alignSelf:'center',
      marginLeft:10
    }, 
    shareButton: {
      marginTop:10,
      height:45,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius:30,
      backgroundColor: "#00BFFF",
    },
  });