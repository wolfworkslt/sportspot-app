import React from 'react';
import {
  StyleSheet, View, Text, ActivityIndicator,
} from 'react-native';

/**
 * @component LoadingScreen
 * @props
 * */
const LoadingScreen = () => (
  <View style={styles.container}>
    <ActivityIndicator size="small" />
    <Text>Kraunama...</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    justifyContent: 'center',
  },
});

export default LoadingScreen;
