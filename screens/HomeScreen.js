import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions, 
  Button,
  Alert,
  FlatList,
  RefreshControl
} from 'react-native';
import {Camera, Permissions, BarCodeScanner} from 'expo';
import ApiClient from '../helpers/ApiClient';
import CardNumber from "../helpers/CardNumber";
import LoadingScreen from './LoadingScreen';
import NavigationService from '../navigation/NavigationService';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  state = {  
    memberInfo: null,
    loading: false,
    cardNo: null,
    menuItems: null,
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
    cameraOn: false,
    checkNumber: null      
  };  

  constructor(props) {
    super(props);
  } 
  
  componentDidMount() {

  }


  async componentWillMount() {
    const cardNo = await CardNumber.checkForCardNo();
    const { status } = await Permissions.askAsync(Permissions.CAMERA);

    this.setState({
      hasCameraPermission: status === 'granted',
      memberInfo: null,
      cardNo: cardNo,
      loading: false,
      menuItems: [
        {id:1, title: "Jūs",   view: 'Profile',   color:"#FF4500", subtitle: 'Narystės informacija',  image: "name"},
        {id:1, title: "Programos",  view: 'Programs',  color:"#87CEEB", subtitle: 'Individualios programos',  image: "fitness"},
        {id:2, title: "Mokėjimai",  view: 'Payments',  color:"#4682B4", subtitle: 'Paskutiniai mokėjimai', image: "money"} ,
        {id:3, title: "Nustatymai", view: 'Setup',   color:"#191970", subtitle: 'Prijunkite nario kortele',  image: "settings"} ,
      ]
    });
  }
  
  async runCardNumberCheck(cardNo) {
    
    await ApiClient.checkCardNo(cardNo).then(res => {
      if (res.data.OK) {
        CardNumber.storeCardNo(cardNo);
        this.setState({
          cardNo: cardNo,
          cameraOn: false,
          checkNumber: null
        });
      } else {
        this.setState({
          loading: false,
          cameraOn: true,
          checkNumber: null
        });
      }
    });
  }  

  render() {
    if (this.state.loading) {
      return <LoadingScreen />;
    } else {
      return (
        <View style={styles.container}>
          <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          {this.menuOrSetupContent()}
          </ScrollView>
        </View>
      );
    }
  }

  menuOrSetupContent()
  {
    if (this.state.checkNumber) {
      this.runCardNumberCheck(this.state.checkNumber);
      return (
        <View style={styles.helpContainer}>
          <Text style={styles.helpLinkText}>Prašome palaukti kortelės numeris tikrinamas...</Text>
        </View>
      );
    } else {
      if (this.state.cameraOn)  {
        return (
          <View style={styles.container}>
            <BarCodeScanner
              onBarCodeRead={this._handleBarCodeRead}
              autoFocus={Camera.Constants.AutoFocus.on}
              style={{
                height: Dimensions.get('window').height/2,
                width: Dimensions.get('window').width,
                marginBottom: 20
              }}
            />

            <View style={styles.helpContainer}>
                <Text style={styles.cameraLabel}>Nukreipkite fotoaparatą į kortelės barkodą.</Text>
            </View>
          </View>
        );
      } else {
        return this.menuContent();      
      }
    } 
  }

  menuContent() {
    return (
      <View style={styles.container}>
        <FlatList style={styles.list}
          contentContainerStyle={styles.listContainer}
          data={this.state.menuItems}
          horizontal={false}
          numColumns={2}
          keyExtractor= {(item) => {
            return item.id;
          }}
          renderItem={({item}) => {
            return (
              <TouchableOpacity style={[styles.card, {backgroundColor:item.color}]} onPress={() => {this.goToView(item.view)}}>
                <View style={styles.cardHeader}>
                  <Text style={styles.title}>{item.title}</Text>
                </View>
                <Image style={styles.cardImage} source={this.getImageSrc(item.image)}/>
                <View style={styles.cardFooter}>
                  <Text style={styles.subTitle}>{item.subtitle}</Text>
                </View>
              </TouchableOpacity>
            )
          }}/>
      </View>
    );
  }

  getImageSrc(image) {

    switch(image) {
      case 'name':
        return require('../assets/images/name.png');
      break;
      case 'fitness':
        return require('../assets/images/fitness.png');
      break;
      case 'money':
        return require('../assets/images/money.png');
      break;
      case 'settings':
        return require('../assets/images/settings.png');
      break;
    }  
  }

  goToView(what) {
    if (what=='Setup') {
      this.goSetup();  
    } else {
      if (this.state.cardNo) {
        NavigationService.navigate(what, {cardNo: this.state.cardNo});
      } else {
        this.goSetup();
      } 
    }
  }

  goSetup() {
    this.setState({ 
      cameraOn: true,
    });
  }

  _handleBarCodeRead = data => {
    Alert.alert(
      'Ačiū. Numeris nuskaitytas.',
      data.data
    );
  
    this.setState({
      loading: false,
      cameraOn: false,
      checkNumber: data.data
    });
  };  
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    marginTop:20,
  },
  list: {
    //paddingHorizontal: 5,
    backgroundColor:"#E6E6E6",
  },
  listContainer:{
    alignItems:'center'
  },
  /******** card **************/
  card:{
    marginHorizontal:2,
    marginVertical:2,
    flexBasis: '48%',
  },
  cardHeader: {
    paddingVertical: 17,
    paddingHorizontal: 16,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: 'row',
    alignItems:"center", 
    justifyContent:"center"
  },
  cardContent: {
    paddingVertical: 12.5,
    paddingHorizontal: 16,
  },
  cardFooter:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 12.5,
    paddingBottom: 25,
    paddingHorizontal: 16,
    borderBottomLeftRadius: 1,
    borderBottomRightRadius: 1,
  },
  cardImage:{
    height: 70,
    width: 70,
    alignSelf:'center'
  },
  title:{
    fontSize:16,
    flex:1,
    color:"#FFFFFF",
    fontWeight:'bold'
  },
  subTitle:{
    fontSize:12,
    flex:1,
    color:"#FFFFFF",
  },
  icon:{
    height: 20,
    width: 20, 
  },
  helpContainer: {
    flex:1,
    marginTop:20,
  },
  cameraLabel: {
    fontSize:25,
    fontWeight:'bold',
    color:"#FFFFFF",
  }
});     
