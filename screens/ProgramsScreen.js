import React from 'react';
import { StyleSheet, FlatList, View, Text, TouchableOpacity } from 'react-native';
import ApiClient from '../helpers/ApiClient';
import LoadingScreen from './LoadingScreen';
import NavigationService from '../navigation/NavigationService';

export default class ProgramsScreen extends React.Component {
  static navigationOptions = {
    title: 'Treniruočių programos',
  };

  state = {  
    loading: true,
    cardNo: null,
    data: null,
    title: null,
    content: null
  }; 

  constructor(props) {
    super(props);
  } 
  
  componentDidMount() {
    const { navigation } = this.props;

    this.setState({
      cardNo: navigation.getParam('cardNo', null),
      loading: true
      }, () => {
        this.fetchData();
    });
  }     

  componentWillMount() {
  
    this.setState({
      cardNo: null,
      loading: false,
      data: null,
      title: null,
      content: null      
    });
  } 

  async fetchData() {
    
    const cardNo = this.state.cardNo;
    if (cardNo) {
      await ApiClient.getMemberPrograms(cardNo).then(res => {
        this.setState({
          loading: false,
          data: res.data.data
        });  
      });
    }
  }   
  
  _onRefresh = () => {
    this.state.loading = true;
    this.fetchData(0);
  } 

  openText(title, content) {
    NavigationService.navigate('Article', {title: title, content: content});
  }

  render() {
    if (this.state.loading) {
      return <LoadingScreen />;
    } else {
        return (
          <View style={styles.container}>
            <FlatList
              data={this.state.data}
              showsVerticalScrollIndicator={false}
              renderItem={({item}) =>
              <View style={styles.eventList}>  
                  <TouchableOpacity onPress={()=> {this.openText(item.title, item.content)} }>
                    <View style={styles.eventBox}>
                      <View style={styles.eventContent}>
                        <Text  style={styles.eventTime}>{item.title}</Text>
                      </View>
                    </View>
                  </TouchableOpacity>            
              </View>
              }
              keyExtractor={item => item.id+''}
            />
          </View>
        );
    }
  }
}

const styles = StyleSheet.create({
  container:{
    backgroundColor: "#87CEEB",
  },
  eventList:{
    marginTop:10,
  },
  eventBox: {
    padding:10,
    // marginTop:5,
    marginBottom:5,
    flexDirection: 'row',
  },
  eventDate:{
    flexDirection: 'column',
  },
  eventDay:{
    fontSize:50,
    color: "#0099FF",
    fontWeight: "600",
  },
  eventMonth:{
    fontSize:16,
    color: "#0099FF",
    fontWeight: "600",
  },
  eventContent: {
    flex:1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    // marginLeft:10,
    backgroundColor: '#FFFFFF',
    padding:10,
    borderRadius:10
  },
  description:{
    fontSize:15,
    color: "#646464",
  },
  eventTime:{
    fontSize:18,
    color:"#151515",
  },
  userName:{
    fontSize:16,
    color:"#151515",
  },
});