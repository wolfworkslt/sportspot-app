import React from 'react';
import { ScrollView, StyleSheet, RefreshControl, FlatList, View, Text, Image, ListView, TouchableOpacity } from 'react-native';
import ApiClient from '../helpers/ApiClient';
import LoadingScreen from './LoadingScreen';

export default class ProfileScreen extends React.Component {
  static navigationOptions = {
    title: 'Abonento informacija',
  };

  state = {  
    loading: true,
    cardNo: null,
    offset: 0,
    data:  null,
    memberInfo:  null 
  };

  constructor(props) {
    super(props);
  } 

  componentDidMount() {
    const { navigation } = this.props;

    this.setState({
      cardNo: navigation.getParam('cardNo', null),
      loading: true
      }, () => {
        this.fetchData();
    });
  }  

  componentWillMount() {
    
    this.setState({
      cardNo: null,
      loading: true,
      data: null,
      memberInfo:  null
    });
  }

  async fetchData() {
    
    const cardNo = this.state.cardNo;
    if (cardNo) {
      await ApiClient.getMemberData(cardNo).then(res => {
        this.setState({
          memberInfo: res.data.data,
          loading: false
        });  
      });
    }
  } 
  
  _onRefresh = () => {
    this.state.loading = true;
    this.fetchData();
  } 

  render() {
    if (this.state.loading) {
      return <LoadingScreen />;
    } else {
      return (
        <View style={styles.container}>
            <View style={styles.header}>
              <View style={styles.headerContent}>
                  <Text style={styles.username}>{this.state.memberInfo.fullName}</Text>
              </View>
            </View>
  
            <View style={styles.body}>
              <TouchableOpacity>
                    <View style={styles.box}>
                      <Image style={styles.icon} source={this.getImageSrc('calendar')}/>
                      <Text style={styles.title}>Narys nuo:</Text>
                    </View>
                    <View style={styles.boxCenter}>
                      <Text style={styles.titleCenter}>{this.state.memberInfo.joinDate}</Text>
                    </View>                   
              </TouchableOpacity>
              <TouchableOpacity>
                    <View style={styles.box}>
                      <Image style={styles.icon} source={this.getImageSrc('ok')}/>
                      <Text style={styles.title}>Nario statusas: </Text>
                    </View>
                    <View style={styles.boxCenter}>
                      <Text style={styles.titleCenter}>{ this.state.memberInfo.inactive?'Sustabdyta narystė':'Aktyvus'}</Text>
                    </View>     
              </TouchableOpacity>
              <TouchableOpacity>
                    <View style={styles.box}>
                      <Image style={styles.icon} source={this.getImageSrc('calendar')}/>
                      <Text style={styles.title}>Paskutinio mokėjimo periodas: </Text>
                    </View>
                    <View style={styles.boxCenter}>
                      <Text style={styles.titleCenter}>{this.state.memberInfo.lastPaymentPeriod}</Text>
                    </View>                         
              </TouchableOpacity>  
              <TouchableOpacity>
                    <View style={styles.box}>
                      <Image style={styles.icon} source={this.getImageSrc('key')}/>
                      <Text style={styles.title}>Nario kortelės numeris: </Text>
                    </View>
                    <View style={styles.boxCenter}>
                      <Text style={styles.titleCenter}>{this.state.memberInfo.cardNumber}</Text>
                    </View> 
              </TouchableOpacity>                                         
            </View>
        </View>
      );
    }
  }

  getImageSrc(image) {

    switch(image) {
      case 'key':
        return require('../assets/images/key.png');
      break;
      case 'calendar':
        return require('../assets/images/calendar.png');
      break;
      case 'ok':
        return require('../assets/images/ok.png');
      break;
    }  
  }  
}

const styles = StyleSheet.create({
  header:{
    backgroundColor: "#FF4500",
  },
  headerContent:{
    padding:30,
    alignItems: 'center',
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "#FF6347",
    marginBottom:10,
  },
  icon:{
    width: 40,
    height: 40,
  },
  title:{
    paddingTop:5,
    fontSize:18,
    color:"#FF4500",
    marginLeft:4
  },
  titleCenter: {
    paddingTop:5,
    fontSize:20,
    alignSelf:'center',
    color: 'black',
    fontWeight: "bold",
  },
  btn:{
    marginLeft: 'auto',
     width: 40,
    height: 40,
  },
  body: {
    backgroundColor :"#E6E6FA",
  },
  box: {
    padding:5,
    marginBottom:2,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    shadowColor: 'black',
    shadowOpacity: .2,
    shadowOffset: {
      height:1,
      width:-2
    },
    elevation:2
  },
  boxCenter: {
    padding:5,
    marginBottom:2,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    justifyContent:"center",
    shadowColor: 'black',
    shadowOpacity: .2,
    shadowOffset: {
      height:1,
      width:-2
    },
    elevation:2,
    alignItems: 'center',
  },
  username:{
    fontSize:25,
    alignSelf:'center',
    fontWeight: "bold",
    marginLeft:10
  }
});