import * as Expo from 'expo';

const {
  Constants: { manifest },
} = Expo;
export const releaseChannel = manifest.releaseChannel || 'default';

const defaultConfig = {};

const Config = {
  default: {
    apiURL: 'http://192.168.1.67/api/v1/',    
    // apiURL: 'http://192.168.43.208/api/v1/',
    // apiURL: 'http://192.168.1.70/api/v1/',
    // apiURL: 'http://192.168.100.55/api/v1/',r
    // apiURL: 'http://192.168.1.69/api/v1/',
    apiKEY: 'SUperMEgaKey115',
    debug: true,
    gaId: false,
    ...defaultConfig,
  },
  staging: {
    apiURL: 'http://192.168.1.67/api/v1/',    
    // apiURL: 'http://192.168.43.208/api/v1/',
    // apiURL: 'http://192.168.1.70/api/v1/',
    // apiURL: 'http://192.168.100.55/api/v1/',
    // apiURL: 'http://192.168.1.69/api/v1/',
    apiKEY: 'SUperMEgaKey115',
    debug: true,
    gaId: false,
    ...defaultConfig,
  },
  production: {
    apiURL: 'http://192.168.1.67/api/v1/',    
    // apiURL: 'http://192.168.43.208/api/v1/',
    // apiURL: 'http://192.168.1.70/api/v1/',
    // apiURL: 'http://192.168.100.55/api/v1/',
    // apiURL: 'http://192.168.1.69/api/v1/',
    apiKEY: 'SUperMEgaKey115',
    debug: false,
    gaId: '',
    ...defaultConfig,
  },
}[releaseChannel];

export default Config;
